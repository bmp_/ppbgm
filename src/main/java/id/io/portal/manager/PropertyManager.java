/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.portal.manager; 

import id.io.portal.util.log.AppLogger;
import java.io.File;
import java.util.Properties;
import javax.inject.Singleton;
import id.io.portal.util.property.Property;

@Singleton
public class PropertyManager {

    private final AppLogger log;
    private static PropertyManager instance;

    public static File BASE_DIRECTORY;
    public static File ASSET_DIRECTORY;
    private Properties properties;

    private PropertyManager() {
        log = new AppLogger(this.getClass());
        try {
            properties = new Properties();
            properties.load(PropertyManager.class.getClassLoader().getResourceAsStream(Property.CONFIGURATION_FILE));

        } catch (Exception ex) {
            log.error("", ex);
        }
    }

    public static PropertyManager getInstance() {
        if (instance == null) {
            instance = new PropertyManager();
        }
        return instance;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public boolean getBoolProperty(String key) {
        return Boolean.getBoolean(properties.getProperty(key, "false"));

    }

}
