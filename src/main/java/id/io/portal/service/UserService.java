/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.service;

import id.io.portal.controller.UserController;
import id.io.portal.manager.EncryptionManager;
import id.io.portal.util.constant.ConstantHelper;
import java.util.Iterator;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService extends BaseService {

    private UserController userController = new UserController();

    public UserService() {
        log = getLogger(this.getClass());
    }

    @GET
    public Response getUsers() {
        log.debug("GET api/user");

        JSONObject response = new JSONObject();
        try {
            response = userController.getUsers();
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_create_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User Level cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(String jsonRequest) {
        log.debug("POST api/user");
        JSONObject response = new JSONObject();
        try {
            response = userController.createUser(new JSONObject(jsonRequest));
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_create_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @POST
    @Path("/unlock/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response lockUser(@PathParam("userId") String userId) {
        log.debug("POST api/user/unlock/{userId}");
        JSONObject response = new JSONObject();
        try {
            response = userController.lockUser(userId);
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_lock_unlock_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Lock / Unclock User cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @DELETE
    @Path("/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("userId") String userId) {
        log.debug("DELETE api/user/{userId}");
        JSONObject response = new JSONObject();
        try {
            response = userController.deleteUser(userId);
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_delete_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Delete User cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @POST
    @Path("/authenticate")
    public Response authenticate(String jsonRequest) {
        log.debug("GET api/user/levels");
        JSONObject response = new JSONObject();

        JSONObject json = new JSONObject(jsonRequest);
        try {
            if (json.length() != 0) {
                Iterator<?> keys = json.keys();

                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    String encryptedPassword =  EncryptionManager.encrypt(json.getString("password"));
                    response = userController.authentication(key, json.get(key).toString(), encryptedPassword);
                }
            }

        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_authenticate_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Authenticate User cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @POST
    @Path("/levels")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUserLevels(String jsonRequest) {
        log.debug("POST api/user/levels");
        JSONObject response = new JSONObject();
        try {
            response = userController.createUserLevel(new JSONObject(jsonRequest));
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_create_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User Level cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @GET
    @Path("/levels")
    public Response getUserLevels() {
        log.debug("GET api/user/levels");

        return Response.ok(userController.getUserLevels()).build();
    }

    @GET
    @Path("/levels/{levelId}")
    public Response getuserLevel(@PathParam("levelId") String levelId) {
        log.debug("GET api/user/levels/{levelId}");
        JSONObject response = new JSONObject();
        try {
            response = userController.getUserLevel(levelId);
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_get_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Get User Level cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @PUT
    @Path("/levels/{levelId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUserLevel(@PathParam("levelId") String levelId, String json) {
        log.debug("PUT api/user/levels/{levelId}");
        JSONObject response = new JSONObject();
        try {
            response = userController.updateUserLevel(levelId, new JSONObject(json));
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_update_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Update User Level cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @POST
    @Path("/levels/{levelId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response lockUserLevels(@PathParam("levelId") String levelId) {
        log.debug("POST api/user/levels/{levelId}");
        JSONObject response = new JSONObject();
        try {
            response = userController.lockUserLevel(levelId);
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_lock_unlock_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Lock / Unclock User Level cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @DELETE
    @Path("/levels/{levelId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUserLevel(@PathParam("levelId") String levelId) {
        log.debug("DELETE api/user/levels/{levelId}");
        JSONObject response = new JSONObject();
        try {
            response = userController.deleteUserLevel(levelId);
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_delete_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Delete User Level cause :" + ex.getMessage());
        }
        return Response.status((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

}
