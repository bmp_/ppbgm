/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.console;

import java.util.Random;

/**
 *
 * @author bmp
 */
public class GenerateRandom {

    public static void main(String[] args) {

        Random rnd = new Random();
        int number = rnd.nextInt(999999999);

        // this will convert any number sequence into 6 character.
        System.out.println(String.format("%06d", number));
    }

}
