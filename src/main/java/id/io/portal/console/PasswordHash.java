/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.portal.console;

import id.io.portal.manager.EncryptionManager; 

public class PasswordHash {

    public static void main(String[] args) {

        EncryptionManager.init();
        System.out.println(EncryptionManager.encrypt("P@ssw0rd"));
        System.out.println(EncryptionManager.decrypt("Ri1eBdpqvxt6KWfkD8ZWNQ=="));
        EncryptionManager.shutdown();
    }
        
        

}
