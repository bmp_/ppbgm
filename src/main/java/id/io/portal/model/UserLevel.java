/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UserLevel {

    private String levelid;
    private String levelcode;
    private String levelname;
    private String description;
    private int isactive;

    public UserLevel() {
    }

    public UserLevel(String levelid, String levelcode, String levelname, String description, int isactive) {
        this.levelid = levelid;
        this.levelcode = levelcode;
        this.levelname = levelname;
        this.description = description;
        this.isactive = isactive;
    }

    public String getLevelid() {
        return levelid;
    }

    public void setLevelid(String levelid) {
        this.levelid = levelid;
    }

    public String getLevelcode() {
        return levelcode;
    }

    public void setLevelcode(String levelcode) {
        this.levelcode = levelcode;
    }

    public String getLevelname() {
        return levelname;
    }

    public void setLevelname(String levelname) {
        this.levelname = levelname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

}
