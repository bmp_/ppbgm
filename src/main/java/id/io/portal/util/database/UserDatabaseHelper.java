/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.util.database;

import id.io.portal.model.AuthenticatedUser;
import id.io.portal.model.User;
import id.io.portal.model.UserLevel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdbi.v3.core.Handle;

public class UserDatabaseHelper extends BaseDatabaseHelper {

    public UserDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean createUser(User user) {
        String methodName = "createUser";
        boolean result = false;
        start(methodName);

        final String userSql = "INSERT INTO manageduser (userid, username, password, memberid) VALUES( :userid, :username, :password, :memberid);";
        final String memberSql = "INSERT INTO usermember (memberid, givenname, fullname, email, phone, avatar,addressid, levelid) VALUES( :memberid, :givenname, :fullname, :email, :phone, :avatar, :addressid, :levelid);";
        final String addressSql = "INSERT INTO useraddress (addressid, address, zone, geolocation) VALUES( :addressid, :address, :zone, :geolocation );";
        try (Handle handle = getHandle()) {

            int row = handle.createUpdate(addressSql)
                    .bind("addressid", user.getUserid())
                    .bind("address", "NA")
                    .bind("zone", "NA")
                    .bind("geolocation", "NA").execute();

            if (row != 0) {
                row = handle.createUpdate(memberSql)
                        .bind("memberid", user.getUserid())
                        .bind("givenname", user.getGivenname())
                        .bind("fullname", user.getFullname())
                        .bind("email", user.getEmail())
                        .bind("phone", user.getPhone())
                        .bind("avatar", user.getAvatar())
                        .bind("addressid", user.getUserid())
                        .bind("levelid", user.getLevelid()).execute();
                if (row != 0) {
                    row = handle.createUpdate(userSql)
                            .bind("userid", user.getUserid())
                            .bind("username", user.getUsername())
                            .bind("password", user.getPassword())
                            .bind("memberid", user.getUserid()).execute();
                    if (row != 0) {
                        result = true;
                    }
                }
            }

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorCreateUser : " + ex);
        }

        completed(methodName);
        return result;
    }

    public List<User> getUsers() {
        String methodName = "getUsers";
        List<User> userList = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT manageduser.userid, manageduser.username, usermember.givenname, usermember.fullname, usermember.email, "
                + "usermember.phone, usermember.avatar, usermember.levelid, useraddress.address, useraddress.zone, useraddress.geolocation, manageduser.isactive, "
                + "usermember.createdt  FROM manageduser INNER JOIN usermember ON manageduser.memberid=usermember.memberid INNER JOIN useraddress ON usermember.addressid=useraddress.addressid;";
        try (Handle handle = getHandle()) {
            userList = handle.createQuery(sql).mapToBean(User.class).list();
        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorListUser : " + ex);
        }

        completed(methodName);
        return userList;
    }

    public String lockUser(String userId) {
        String result = "failed";
        String methodName = "active/InactiveUser";
        start(methodName);

        final String isactive = "SELECT isactive FROM manageduser WHERE userid= :userid;";
        try (Handle handle = getHandle()) {
            int status = handle.createQuery(isactive).bind("userid", userId).mapTo(Integer.class).one();
            if (status == 0) {
                final String sql = "UPDATE manageduser SET  isactive= 1 WHERE userid= :userid;";
                int row = handle.createUpdate(sql)
                        .bind("userid", userId).execute();
                if (row != 0) {
                    result = "active";
                }
            } else {
                final String sql = "UPDATE manageduser SET  isactive= 0 WHERE userid= :userid;";
                int row = handle.createUpdate(sql)
                        .bind("userid", userId).execute();
                if (row != 0) {
                    result = "inactive";
                }
            }
        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorActive/InactiveUser : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean deleteUser(String userId) {
        boolean result = false;
        String methodName = "deleteUser";
        start(methodName);

        final String removeAddress = "DELETE FROM useraddress WHERE addressid = :userid;";
        final String removeMember = "DELETE FROM usermember WHERE memberid = :userid;";
        final String removeUser = "DELETE FROM manageduser WHERE userid = :userid;";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(removeUser).bind("userid", userId).execute();
            if (row != 0) {
                row = handle.createUpdate(removeMember).bind("userid", userId).execute();
                if (row != 0) {
                    row = handle.createUpdate(removeAddress).bind("userid", userId).execute();
                    if (row != 0) {
                        result = true;
                    }
                }
            }

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorDeleteUserLevel : " + ex);
        }

        completed(methodName);
        return result;

    }

    public AuthenticatedUser authenticate(String username, String email, String phone, String password) {
        String methodName = "authenticate";
        start(methodName);
        AuthenticatedUser user = new AuthenticatedUser();
        final String sql = "SELECT manageduser.userid, manageduser.username, usermember.email, usermember.phone,  userlevel.levelname "
                + " FROM manageduser "
                + " INNER JOIN usermember ON manageduser.memberid=usermember.memberid "
                + " INNER JOIN userlevel ON usermember.levelid=userlevel.levelid "
                + " WHERE username = :username OR email = :email OR phone = :phone and password = :password";
        try (Handle handle = getHandle()) {

            user = handle.createQuery(sql)
                    .bind("username", username)
                    .bind("email", email)
                    .bind("phone", phone)
                    .bind("password", password)
                    .mapToBean(AuthenticatedUser.class).first();

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - authenticate : " + ex);
        }

        completed(methodName);
        return user;
    }

    public String getUserId(String username, String email, String phone) {
        String methodName = "getUserId";
        start(methodName);
        String userId = null;
        String sql = null;
        String trigger = null;
        if (username != null) {
            sql = "SELECT userid FROM manageduser WHERE username = :trigger;";
            trigger = username;
        } else if (email != null) {
            sql = "SELECT memberid FROM usermember WHERE email = :trigger;";
            trigger = email;
        } else if (phone != null) {
            sql = "SELECT memberid FROM usermember WHERE phone = :trigger;";
            trigger = phone;
        }
        try (Handle handle = getHandle()) {
            userId = handle.createQuery(sql).bind("trigger", trigger).mapTo(String.class).one();

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - getUserId : " + ex);
        }

        completed(methodName);
        return userId;
    }

    public boolean createUserLevel(UserLevel userlevel) {
        String methodName = "createUserLevel";
        boolean result = false;
        start(methodName);
        final String sql = "INSERT INTO userlevel (levelid, levelcode, levelname, description) VALUES(:levelid, :levelcode, :levelname, :description);";

        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind("levelid", userlevel.getLevelid())
                    .bind("levelcode", userlevel.getLevelcode())
                    .bind("levelname", userlevel.getLevelname())
                    .bind("description", userlevel.getDescription()).execute();
            if (row != 0) {
                result = true;
            }

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorCreateUserLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

    public List<UserLevel> getUserLevels() {
        String methodName = "getUserLevels";
        List<UserLevel> userLevelList = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT levelid, levelcode, levelname, description, isactive FROM userlevel;";
        try (Handle handle = getHandle()) {
            userLevelList = handle.createQuery(sql).mapToBean(UserLevel.class).list();
        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorListUserLevel : " + ex);
        }

        completed(methodName);
        return userLevelList;
    }

    public boolean updateUserLevel(UserLevel userLevel) {
        String methodName = "updateUserLevel";
        start(methodName);

        final String sql = "UPDATE userlevel SET  levelname= :levelname, description= :description WHERE levelid= :levelid;";
        boolean result = false;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind("levelname", userLevel.getLevelname())
                    .bind("description", userLevel.getDescription())
                    .bind("levelid", userLevel.getLevelid()).execute();
            if (row != 0) {
                result = true;
            }

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), "- errorUpdateUserLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

    public UserLevel getUserLevel(String levelId) {
        UserLevel userLevel = new UserLevel();
        String methodName = "getUserLevel";
        start(methodName);

        final String sql = "SELECT levelid, levelcode, levelname, description, isactive FROM userlevel WHERE levelid= :levelid;";
        try (Handle handle = getHandle()) {
            userLevel = handle.createQuery(sql).bind("levelid", levelId).mapToBean(UserLevel.class).first();
        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorGetUserLevel : " + ex);
        }

        completed(methodName);
        return userLevel;

    }

    public String lockUserLevel(String levelId) {
        String result = "failed";
        String methodName = "active/InactiveUserLevel";
        start(methodName);

        final String isactive = "SELECT isactive FROM userlevel WHERE levelid= :levelid;";
        try (Handle handle = getHandle()) {
            int status = handle.createQuery(isactive).bind("levelid", levelId).mapTo(Integer.class).one();
            if (status == 0) {
                final String sql = "UPDATE userlevel SET  isactive= 1 WHERE levelid= :levelid;";
                int row = handle.createUpdate(sql)
                        .bind("levelid", levelId).execute();
                if (row != 0) {
                    result = "active";
                }
            } else {
                final String sql = "UPDATE userlevel SET  isactive= 0 WHERE levelid= :levelid;";
                int row = handle.createUpdate(sql)
                        .bind("levelid", levelId).execute();
                if (row != 0) {
                    result = "inactive";
                }
            }
        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorActive/InactiveUserLevel : " + ex);
        }

        completed(methodName);
        return result;
    }

    public boolean deleteUserLevel(String levelId) {
        boolean result = false;
        String methodName = "deleteUserLevel";
        start(methodName);

        final String sql = "DELETE FROM userlevel WHERE levelid= :levelid;";
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind("levelid", levelId).execute();
            if (row != 0) {
                result = true;
            }

        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorDeleteUserLevel : " + ex);
        }

        completed(methodName);
        return result;

    }

}
