/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.portal.util.constant; 

public class ConstantHelper {

    // REST Endpoints
    public static final String HTTP_STATUS_CODE         = "statusCode";
    public static final String HTTP_RESPONSE            = "response";
    public static final String HTTP_REASON              = "reason";
    public static final String HTTP_CODE                = "code";
    public static final String HTTP_MESSAGE             = "message";
    public static final String HTTP_SUCCESSFUL          = "successful";

   //User Configuration
    public static final String USER_ADMINISTRATOR       = "Administrator";
    
    
}
