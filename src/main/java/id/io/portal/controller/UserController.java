/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.portal.controller;

import id.io.portal.manager.EncryptionManager;
import id.io.portal.model.AuthenticatedUser;
import id.io.portal.model.User;
import id.io.portal.model.UserLevel;
import id.io.portal.util.constant.ConstantHelper;
import id.io.portal.util.database.UserDatabaseHelper;
import java.util.List;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserController extends BaseController {

    private UserDatabaseHelper userDatabaseHelper;

    public UserController() {
        log = getLogger(this.getClass());
        this.userDatabaseHelper = new UserDatabaseHelper();
    }

    public JSONObject authentication(String filter, String values, String password) {
        JSONObject response = new JSONObject();

        AuthenticatedUser user = new AuthenticatedUser();
        switch (filter) {
            case "username":
                user = userDatabaseHelper.authenticate(values, null, null, password);
                break;
            case "email":
                user = userDatabaseHelper.authenticate(null, values, null, password);
                break;
            case "phone":
                user = userDatabaseHelper.authenticate(null, null, values, password);
                break;
            default:
                break;
        }

        if (user.getUserid() != null) {
            boolean admin = false;
            if (user.getLevelname() == null ? ConstantHelper.USER_ADMINISTRATOR == null : user.getLevelname().equals(ConstantHelper.USER_ADMINISTRATOR));
            {
                admin = true;
            }
            user.setAdmin(admin);
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_RESPONSE, new JSONObject(user));
        }
        else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "wrong_username_or_password");
            response.put(ConstantHelper.HTTP_MESSAGE, "Login Failed : Wrong credentials");
        }

        return response;
    }

    public JSONObject getUsers() {
        String methodName = "getUserLevels";
        start(methodName);

        List<User> userList = userDatabaseHelper.getUsers();

        JSONArray userArr = new JSONArray();
        for (int i = 0; i < userList.size(); i++) {
            UserLevel userLevel = userDatabaseHelper.getUserLevel(userList.get(i).getLevelid());

            JSONObject userObj = new JSONObject();
            userObj.put("userId", userList.get(i).getUserid());
            userObj.put("userName", userList.get(i).getUsername());
            userObj.put("givenName", userList.get(i).getGivenname());
            userObj.put("fullName", userList.get(i).getFullname());

            userObj.put("level", userLevel.getLevelname());

            JSONObject addressObj = new JSONObject();
            addressObj.put("address", userList.get(i).getAddress());
            addressObj.put("zone", userList.get(i).getZone());
            addressObj.put("geoLocation", userList.get(i).getGeolocation());

            userObj.put("address", addressObj);

            JSONObject contactObj = new JSONObject();
            contactObj.put("email", userList.get(i).getEmail());
            contactObj.put("phone", userList.get(i).getPhone());

            userObj.put("contact", contactObj);

            userObj.put("avatar", userList.get(i).getAvatar());
            String isActive = null;
            if (userList.get(i).getIsactive().equals("1")) {
                isActive = "active";
            } else if (userList.get(i).getIsactive().equals("0")) {
                isActive = "inactive";
            }
            userObj.put("isActive", isActive);
            userObj.put("createDt", userList.get(i).getCreatedt());

            userArr.put(userObj);
        }
        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_RESPONSE, (Object) userArr);
        completed(methodName);
        return response;
    }

    public JSONObject createUser(JSONObject json) {
        String methodName = "createUser";
        start(methodName);

        JSONObject response = new JSONObject();
        if (json.length() != 0) {
            User user = new User();
            user.setUserid(generateUUID(json.getString("userName")).toString());
            user.setUsername(json.getString("userName"));
            String encryptedPassword = EncryptionManager.encrypt(json.getString("password"));
            user.setPassword(encryptedPassword);
            user.setFullname(json.getString("fullName"));
            user.setGivenname(json.getString("givenName"));
            user.setPhone(json.getString("phone"));
            user.setEmail(json.getString("email"));
            user.setAvatar(json.getString("avatar"));
            user.setLevelid(json.getString("level"));

            boolean result = userDatabaseHelper.createUser(user);
            if (result) {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
                response.put(ConstantHelper.HTTP_REASON, "create_user_successful");
                response.put(ConstantHelper.HTTP_MESSAGE, "Create User Successful!");
            } else {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
                response.put(ConstantHelper.HTTP_REASON, "error_create_user");
                response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User");
            }

        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_create_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User : No such Data");
        }

        completed(methodName);
        return response;
    }

    public JSONObject lockUser(String userId) {
        String methodName = "lockUser";
        start(methodName);

        JSONObject response = new JSONObject();
        String result = userDatabaseHelper.lockUser(userId);
        if (!result.equals("failed")) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            if (result.equals("active")) {
                response.put(ConstantHelper.HTTP_RESPONSE, "Activate");
                response.put(ConstantHelper.HTTP_MESSAGE, "Activate User Successful!");
            } else if (result.equals("inactive")) {
                response.put(ConstantHelper.HTTP_RESPONSE, "Deactivate");
                response.put(ConstantHelper.HTTP_MESSAGE, "Deactivate User Successful!");
            }
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_activate_deactivate_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Activate / Deactivate User : No such Data");
        }

        completed(methodName);
        return response;
    }

    public JSONObject deleteUser(String userId) {
        String methodName = "deleteUser";
        start(methodName);

        JSONObject response = new JSONObject();
        boolean result = userDatabaseHelper.deleteUser(userId);
        if (result) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_REASON, "delete_user_successful");
            response.put(ConstantHelper.HTTP_MESSAGE, "Delete User Successful!");
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_delete_user");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Delete User : No such Data");
        }

        completed(methodName);
        return response;
    }

    public List<UserLevel> getUserLevels() {
        String methodName = "getUserLevels";
        start(methodName);

        List<UserLevel> assetList = userDatabaseHelper.getUserLevels();

        completed(methodName);
        return assetList;
    }

    public JSONObject getUserLevel(String levelId) {
        String methodName = "getUserLevel";
        start(methodName);

        JSONObject response = new JSONObject();
        UserLevel model = userDatabaseHelper.getUserLevel(levelId);
        if (model != null) {

            JSONObject json = new JSONObject(model);
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_RESPONSE, json);

        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_get_user_level");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Get User Level : No such Data");
        }

        completed(methodName);
        return response;
    }

    public JSONObject createUserLevel(JSONObject json) {
        String methodName = "getUserLevels";
        start(methodName);

        JSONObject response = new JSONObject();
        if (json.length() != 0) {
            UserLevel model = new UserLevel();
            model.setLevelid(generateUUID(json.getString("levelcode")).toString());
            model.setLevelcode(json.getString("levelcode"));
            model.setLevelname(json.getString("levelname"));
            model.setDescription(json.getString("description"));

            boolean result = userDatabaseHelper.createUserLevel(model);
            if (result) {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
                response.put(ConstantHelper.HTTP_REASON, "create_user_leve_successful");
                response.put(ConstantHelper.HTTP_MESSAGE, "Create User Level Successful!");
            } else {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
                response.put(ConstantHelper.HTTP_REASON, "error_create_user_level");
                response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User Level");
            }

        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_create_user_level");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User Level : No such Data");
        }

        completed(methodName);
        return response;
    }

    public JSONObject updateUserLevel(String levelId, JSONObject json) {
        String methodName = "updateUserLevel";
        start(methodName);

        JSONObject response = new JSONObject();
        if (json.length() != 0) {
            UserLevel model = new UserLevel();
            model.setLevelid(levelId);
            model.setLevelname(json.getString("levelname"));
            model.setDescription(json.getString("description"));

            boolean result = userDatabaseHelper.updateUserLevel(model);
            if (result) {
                UserLevel resultModel = userDatabaseHelper.getUserLevel(levelId);
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
                response.put(ConstantHelper.HTTP_REASON, "update_user_leve_successful");
                response.put(ConstantHelper.HTTP_RESPONSE, new JSONObject(resultModel));
            } else {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
                response.put(ConstantHelper.HTTP_REASON, "error_update_user_level");
                response.put(ConstantHelper.HTTP_MESSAGE, "Error Update User Level");
            }

        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_update_user_level");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Update User Level : No such Data");
        }

        completed(methodName);
        return response;
    }

    public JSONObject lockUserLevel(String levelId) {
        String methodName = "lockUserLevel";
        start(methodName);

        JSONObject response = new JSONObject();
        String result = userDatabaseHelper.lockUserLevel(levelId);
        if (!result.equals("failed")) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            if (result.equals("active")) {
                response.put(ConstantHelper.HTTP_RESPONSE, "Activate");
                response.put(ConstantHelper.HTTP_MESSAGE, "Activate User Level Successful!");
            } else if (result.equals("inactive")) {
                response.put(ConstantHelper.HTTP_RESPONSE, "Deactivate");
                response.put(ConstantHelper.HTTP_MESSAGE, "Deactivate User Level Successful!");
            }
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_activate_deactivate_user_level");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Activate / Deactivate User Level : No such Data");
        }

        completed(methodName);
        return response;
    }

    public JSONObject deleteUserLevel(String levelId) {
        String methodName = "deleteUserLevel";
        start(methodName);

        JSONObject response = new JSONObject();
        boolean result = userDatabaseHelper.deleteUserLevel(levelId);
        if (result) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_REASON, "delete_user_level_successful");
            response.put(ConstantHelper.HTTP_MESSAGE, "Delete User Level Successful!");
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, "error_delete_user_level");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Delete User Level : No such Data");
        }

        completed(methodName);
        return response;
    }

}
